# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.9.0] - 2021-12-13
### Added
- Calibration using sqrt function

## [1.8.0] - 2021-11-11
### Added
- Calibration using polynomial approximation

### Removed
- Linear calibration and old non linear calibration
