from math import nan
from random import randint

from epics import caget, caput
from helper import generate_csv, generate_csv_rand, generate_csv_unordered,\
                   PREC

def test_csv(pref, file_name, max_elem):
    """
    Make a test for a random csv file, verifying if the slope and offset were
    correctly calculated
    pref: PV prefix
    file_name: name of csv file to be generated
    max_elem: number maximum of elements to be generated
    """
    elem = randint(2, max_elem) if max_elem > 1 else 1
    (slope, offset) = generate_csv(file_name, elem)
    
    caput(pref+"-FileName", file_name)

    slope_IOC = caget(pref + "-Slope")
    assert slope_IOC != nan

    offset_IOC = caget(pref + "-Offset")
    assert offset_IOC != nan

    diff_slope = abs(slope - slope_IOC)
    diff_offset = abs(offset - offset_IOC)

    assert diff_slope < 10**-(PREC-1)
    assert diff_offset < 10**-(PREC-1)


def test_fitted_line(pref, file_name, max_elem):
    """
    check if fitted line is right
    """
    generate_csv_rand(file_name, max_elem)

    caput(pref+"-FileName", file_name)
   
    # check if the number of elements is correct 
    n_elem = caget(pref + "-InputValues.NORD")
    assert n_elem == max_elem

    # check if the last element on fitted line is correct
    slope = caget(pref + "-Slope")
    assert slope != nan

    offset = caget(pref + "-Offset")
    assert offset != nan

    elems = caget(pref + "-FittedLine")
    assert len(elems) == max_elem

    last_elem = elems[-1]

    elems_dig = caget(pref + "-DigitisedValues")
    assert len(elems_dig) == max_elem

    last_elem_dig = elems_dig[-1]

    diff = abs(last_elem - (last_elem_dig*slope + offset))

    assert diff < 10**-(PREC-1)

def test_one_line(pref, file_name):
    generate_csv(file_name, 1)

    caput(pref + "-FileName", file_name)

    slope = caget(pref + "-Slope")
    assert slope != nan

    offset = caget(pref + "-Offset")
    assert offset != nan

    assert slope == 1 and offset == 0   


def test_unorderd_csv(pref, file_name, max_elem):
    generate_csv_unordered(file_name, max_elem)

    caput(pref + "-FileName", file_name)
   
    stat = caget(pref + "-FileName.STAT")
    sevr = caget(pref + "-FileName.SEVR")

    slope = caget(pref + "-Slope")
    assert slope != nan

    offset = caget(pref + "-Offset")
    assert offset != nan

    assert stat == 2 and sevr == 3 and slope == 1 and offset == 0


def test_last_lines_blank(pref, file_name, max_elem):
    (slope, offset) = generate_csv(file_name, max_elem)

    # add n blank lines at the end
    with open(file_name, "a") as f:
        f.seek(0, 2)
        for i in range(randint(1, max_elem)):
            f.write("\n")
    
    caput(pref+"-FileName", file_name)

    slope_IOC = caget(pref + "-Slope")
    assert slope_IOC != nan

    offset_IOC = caget(pref + "-Offset")
    assert offset_IOC != nan

    diff_slope = abs(slope - slope_IOC)
    diff_offset = abs(offset - offset_IOC)

    assert diff_slope < 10**-(PREC-1)
    assert diff_offset < 10**-(PREC-1)

    
