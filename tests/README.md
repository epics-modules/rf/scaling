# scaling tests

## params

* `--pref`: PV prefix
* `--max` : maximum number of lines on input file
* `--file`: path to input file
* `--channels`: an interval of channels to be used on format "initial,final", this interval will be concatened to pref and all the tests will be executed for each combination pref+channel

## how to run

* Simpler way:
`pytest --pref=LLRF:AI0`

* Other params:
`pytest --pref=LLRF:AI0 --max=30 --file=/home/iocuser/file.csv`

* Using channels:
`pytest --pref=LLRF:AI --chanels=0,9 # will run tests for LLRF:AI0, LLRF:AI1, ..., LLRF:AI9`

