import csv
from random import random

PREC=4
MAXELEM=20
FILENAME="/tmp/input.csv"

def generate_csv(file_name, size):
    """
    Generate a csv file with random numbers, where second column is 
    a list of sorted elements and the first column is a subproduct from
    second (applied a random slope and offset)
    file_name: name of file to generated
    size: number of elements of csv
    return the slope and offset
    """
    slope = round(random(), PREC)
    offset = round(random(), PREC)

    input = []
    for i in range(size):
        input.append(round(random(), PREC))

    input = sorted(input)

    with open(file_name, 'w') as f:
        w = csv.writer(f)
        w.writerow(["# digitilised, input"])
        for i in range(size):
            w.writerow([round(input[i]*slope + offset, PREC), input[i]])
 
    return(slope,offset) 

# Generate a comletely random csv without know the slope and offset
def generate_csv_rand(file_name, size):
    input = []
    digitised = []
    for i in range(size):
        input.append(round(random(), PREC))
        digitised.append(round(random(), PREC))

    input = sorted(input)
    digitised = sorted(digitised)

    with open(file_name, 'w') as f:
        w = csv.writer(f)
        w.writerow(["# digitilised, input"])
        for i in range(size):
            w.writerow([digitised[i], input[i]])

# generate an unordered CSV
def generate_csv_unordered(file_name, size):
    input = []
    digitised = []
    ordered = True

    for i in range(size):
        input.append(round(random(), PREC))
        digitised.append(round(random(), PREC))

    input = sorted(input, reverse = True)
    digitised = sorted(digitised, reverse = True)

    with open(file_name, 'w') as f:
        w = csv.writer(f)
        w.writerow(["# digitilised, input"])
        for i in range(size):
            w.writerow([digitised[i], input[i]])

