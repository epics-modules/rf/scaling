from helper import MAXELEM, FILENAME 

def pytest_addoption(parser):
    parser.addoption("--pref")
    parser.addoption("--file")
    parser.addoption("--max")
    parser.addoption("--channels")

def pytest_generate_tests(metafunc):
    if "pref" in metafunc.fixturenames:
        p = metafunc.config.getoption("pref")
        if p is not None:
            ch = metafunc.config.getoption("channels")
            if ch is not None and len(ch.split(",")) == 2:
                init_ch = int(ch.split(",")[0])
                end_ch = int(ch.split(",")[1])+1
                prefs = []
                for c in range(init_ch, end_ch):
                    prefs.append(p + str(c))
                metafunc.parametrize("pref", prefs)
            else:
                metafunc.parametrize("pref", [p])

    if "file_name" in metafunc.fixturenames:
        f = metafunc.config.getoption("file")
        if f is not None:
            metafunc.parametrize("file_name", [f])
        else:
            metafunc.parametrize("file_name", [FILENAME])

    if "max_elem" in metafunc.fixturenames:
        m = metafunc.config.getoption("max")
        if m is not None:
            m = int(m)
            metafunc.parametrize("max_elem", [m])
        else:
            metafunc.parametrize("max_elem", [MAXELEM])
