#include "scalingPolyPiecewise.h"
#include <iostream>
#include <cmath>
#include <stdexcept>

scalingPolyPiecewise::scalingPolyPiecewise(unsigned int max_order,
                                            const int max_intervals): scalingPoly(max_order), MAX_INTERVALS(max_intervals){
}

scalingPolyPiecewise::~scalingPolyPiecewise() {
    for (auto workspace : workspaces) {
        gsl_multifit_linear_free(workspace);
    }
}

void scalingPolyPiecewise::setX(double * X, unsigned int len){
    scalingPoly::setX(X, len);

    fitPiecewise();
}

void scalingPolyPiecewise::setY(double * Y, unsigned int len){
    scalingPoly::setY(Y, len);

    fitPiecewise();
}


void scalingPolyPiecewise::setStartingPoints(double * points, unsigned int len){
    if (len  > MAX_INTERVALS || len < 1){
        piecewise_calib_valid = false;
        piecewise_calib_en = false;
        return;
    }
    if (m_Y.max_size() < len) {
        m_valid = false;
        m_enable = false;
        return;
    }

    starting_points.resize(len);
    copy(points, points+len, starting_points.begin());

    //re-fit
    fitPiecewise();
}

void scalingPolyPiecewise::setPolynomialDegrees(int * degrees, unsigned int len){
    if (len > MAX_INTERVALS || len < 1){
        piecewise_calib_valid = false;
        piecewise_calib_en = false;
        return;
    }
    if (m_Y.max_size() < len) {
        m_valid = false;
        m_enable = false;
        return;
    }

    polynomial_degrees.resize(len);
    copy(degrees, degrees+len, polynomial_degrees.begin());

    //re-fit
    fitPiecewise();
}


void scalingPolyPiecewise::fitPiecewise(){
     if (starting_points.size() != polynomial_degrees.size() || starting_points.size() > MAX_INTERVALS || starting_points.size() < 1 || polynomial_degrees.size() < 1) {
        piecewise_calib_valid = false;
        return;
    }

    // clear up workspaces and chisq
    chisq_piecewise = 0;

    for (auto* work : workspaces) {
        gsl_multifit_linear_free(work);
    }
    workspaces.clear();
    coefficients.clear();

    scalingPoly::fit();

    // Fit the polynomials for each section
    for (size_t i = 0; i < starting_points.size(); ++i) {
        double start = starting_points[i];
        double end = (i + 1 < starting_points.size()) ? starting_points[i + 1] : m_X.back();

        // Find indices for the range
        std::vector<double> sub_x, sub_y;
        for (size_t j = 0; j < m_X.size(); ++j) {
            if (m_X[j] >= start && m_X[j] < end) {
                sub_x.push_back(m_X[j]);
                sub_y.push_back(m_Y[j]);
            }
        }

        if (sub_x.size() < polynomial_degrees[i] + 1) {
            piecewise_calib_valid = false;
        }

        fitPolynomial(sub_x, sub_y, polynomial_degrees[i]);
    }

    generatePiecewiseFittedLineAndResiduals();
}


void scalingPolyPiecewise::fitPolynomial(const std::vector<double>& sub_x,
                                         const std::vector<double>& sub_y, int degree) {
    size_t n = sub_x.size();
    gsl_matrix* X = gsl_matrix_alloc(n, degree + 1);
    gsl_vector* y = gsl_vector_alloc(n);
    gsl_vector* c = gsl_vector_alloc(degree + 1);
    gsl_matrix* cov = gsl_matrix_alloc(degree + 1, degree + 1);
    gsl_multifit_linear_workspace* work = gsl_multifit_linear_alloc(n, degree + 1);

    for (size_t i = 0; i < n; ++i) {
        gsl_vector_set(y, i, sub_y[i]);
        for (int j = 0; j <= degree; ++j) {
            gsl_matrix_set(X, i, j, pow(sub_x[i], j));
        }
    }

    double chisq;
    gsl_multifit_linear(X, y, c, cov, &chisq, work);

    chisq_piecewise += chisq;

    // Store the coefficients
    std::vector<double> coeff(degree + 1);
    for (int i = 0; i <= degree; ++i) {
        coeff[i] = gsl_vector_get(c, i);
    }
    coefficients.push_back(coeff);
    workspaces.push_back(work);

    gsl_matrix_free(X);
    gsl_vector_free(y);
    gsl_vector_free(c);
    gsl_matrix_free(cov);

    piecewise_calib_valid = true;
}

int scalingPolyPiecewise::findIntervalIndex(double x) const{
    // check if starting points are not empty
    if (starting_points.empty()) {
        return -1;
    }

    if (!starting_points.empty() && x >= starting_points.back()){
        return starting_points.size() - 1;
    }

    auto it = std::upper_bound(starting_points.begin(), starting_points.end(), x);
    return std::max(0, static_cast<int>(it - starting_points.begin()) - 1);
}

void scalingPolyPiecewise::generatePiecewiseFittedLineAndResiduals() {
    if (!piecewise_calib_valid)
        return;

    m_fitted_line_piecewise.resize(0);
    m_residuals_piecewise.resize(0);
    std::vector<double>::iterator it_fit = m_fitted_line_piecewise.begin();
    std::vector<double>::iterator it_res = m_residuals_piecewise.begin();

    int interval_index;
    std::vector<double> poly_coefs;
    int poly_degree;
    for (unsigned int i = 0; i < m_X.size(); i++) {
        interval_index = findIntervalIndex(m_X[i]);
        poly_coefs = coefficients[interval_index];
        poly_degree = polynomial_degrees[interval_index];

        m_fitted_line_piecewise.insert(it_fit, gsl_poly_eval(poly_coefs.data(), poly_degree+1, m_X[i]));
        m_residuals_piecewise.insert(it_res, m_Y[i] - gsl_poly_eval(poly_coefs.data(), poly_degree+1, m_X[i]));
        it_fit = m_fitted_line_piecewise.end();
        it_res = m_residuals_piecewise.end();
    }
}

double scalingPolyPiecewise::calibrate(double x){
    // check if piecewise calibration valid and enabled
    if (!piecewise_calib_valid || !piecewise_calib_en){
        return scalingPoly::calibrate(x);
    }

    // check if the interval was found
    int interval_index = findIntervalIndex(x);

    if (interval_index < 0 || interval_index >= coefficients.size()){
        return 0;
    }

    std::vector<double> poly_coefs =  coefficients[interval_index];
    int poly_degree = polynomial_degrees[interval_index];
    return gsl_poly_eval(poly_coefs.data(), poly_degree + 1, x);
}

void scalingPolyPiecewise::printCoefficients() const {
    for (size_t i = 0; i < coefficients.size(); ++i) {
        std::cout << "Section " << i << " coefficients: ";
        for (double coeff : coefficients[i]) {
            std::cout << coeff << " ";
        }
        std::cout << std::endl;
    }
}

void scalingPolyPiecewise::setPolyCalibEnable(bool enable){
    if (m_valid && m_enable && piecewise_calib_valid){
        piecewise_calib_en = enable;
    }
    else{
        piecewise_calib_en = false;
    }
}
