#ifndef _scaling_sqrt_h
#define _scaling_sqrt_h

#include <cmath>
#include <vector>
using namespace std;

#include "scalingBase.h"

class scalingSqrt : public scalingBase {
    public:
        scalingSqrt():
            scalingBase (),
            m_a(0),
            m_b(0),
            m_c(0)  {};
        //this function doesn't check if is valid the calibration
        //so the call will be faster. It should be checked before
        //call the function
        virtual double calibrate(double value) { 
            return ( sqrt(value+pow(m_b,2)/(4*m_a)-m_c ) - m_b/(2*sqrt(m_a))) / sqrt(m_a)  ;
        }; 
        virtual double * getFittedLine() { return m_fitted_line.data();};
        virtual unsigned int getFittedLineSize() { return m_fitted_line.size();};
        virtual void setCoefficients(double * coef);
        virtual void setX(double * X, unsigned int len);
        virtual void setY(double * Y, unsigned int len){return;};
    protected:
        double m_a, m_b, m_c;
        vector<double> m_fitted_line;
        vector<double> m_X;

        virtual void generateFittedLine();
};

#endif
