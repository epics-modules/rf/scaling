#include <scalingPoly.h>
using namespace std;

void scalingPoly::setX(double * X, unsigned int len){
    if (m_X.max_size() < len) {
        m_valid = false;
        m_enable = false;
        return;
    }
    m_X.resize(len);
    copy(X, X+len, m_X.begin());

    //re-fit
    fit();
}

void scalingPoly::setY(double * Y, unsigned int len){
    if (m_Y.max_size() < len) {
        m_valid = false;
        m_enable = false;
        return;
    }
    m_Y.resize(len);
    copy(Y, Y+len, m_Y.begin());

    //re-fit
    fit();
}

void scalingPoly::setOrder(unsigned int order){
    if (order > m_max_order && order < 1){
        m_valid = false;
        m_enable = false;
        return;
    }
    m_order = order;
    //re-fit
    fit();
}

void scalingPoly::fit(){
    long n;
    unsigned int i, j, status;
    gsl_vector *y, *c;
    gsl_matrix *X, *cov;

    std::vector<double>::iterator it;

    if (m_X.size() != m_Y.size() || m_X.size() <= 1 || m_order <= 0 || m_X.size() < m_order){
        m_valid = false;
        m_enable = false;
        return;
    }

    n = m_X.size();

    y = gsl_vector_alloc (n);
    c = gsl_vector_alloc (m_order+1);
    X   = gsl_matrix_alloc (n, m_order+1);
    cov = gsl_matrix_alloc (m_order+1, m_order+1);

    i = 0;
    for (i = 0; i < n; i++) {
        j = 0;
        for (j = 0; j < m_order+1; j++) {  
          gsl_matrix_set (X, i, j, pow(m_X[i],j));
        }
        gsl_vector_set (y, i, m_Y[i]);
    }

    gsl_multifit_linear_workspace * work = gsl_multifit_linear_alloc (n, m_order+1);

    gsl_set_error_handler_off();
    status = gsl_multifit_linear (X, y, c, cov, &m_chisq, work);
    gsl_multifit_linear_free (work);
    if (status){
        m_valid = false;
        gsl_vector_free (y);
        gsl_vector_free (c);
        gsl_matrix_free (X);
        gsl_matrix_free (cov);
        return;
    }

    m_coefs.resize(0);
    it = m_coefs.begin();

    //i = 0 is the last coefficient
    // get the coefficents
    for (i = 0; i < m_order+1; i++) {
        m_coefs.insert(it, gsl_vector_get(c,i));
        it = m_coefs.end();
    }

    gsl_vector_free (y);
    gsl_vector_free (c);
    gsl_matrix_free (X);
    gsl_matrix_free (cov);

    m_valid = true;
    generateFittedLineAndResiduals();
}


void scalingPoly::generateFittedLineAndResiduals() { 
    if (!m_valid)
        return;

    m_fitted_line.resize(0);
    m_residuals.resize(0);
    std::vector<double>::iterator it_fit = m_fitted_line.begin();
    std::vector<double>::iterator it_res = m_residuals.begin();

    for (unsigned int i = 0; i < m_X.size(); i++) {
        m_fitted_line.insert(it_fit, gsl_poly_eval(m_coefs.data(), m_order+1, m_X[i]));
        m_residuals.insert(it_res, m_Y[i] - gsl_poly_eval(m_coefs.data(), m_order+1, m_X[i]));
        it_fit = m_fitted_line.end();
        it_res = m_residuals.end();
    }
}
