#include <scalingSqrt.h>
using namespace std;

void scalingSqrt::setCoefficients(double * coef) {
    m_c = coef[0];
    m_b = coef[1];
    m_a = coef[2];

    m_valid = true;
    generateFittedLine();
};


void scalingSqrt::generateFittedLine(){ 
    if (!m_valid)
        return;

    std::vector<double>::iterator it;
    m_fitted_line.resize(0);
    it = m_fitted_line.begin();

    for (unsigned int i = 0 ; i < m_X.size() ; i ++){
        m_fitted_line.insert(it, calibrate(m_X[i]));
        it = m_fitted_line.end();
    }
}

void scalingSqrt::setX(double * X, unsigned int len){
    if (m_X.max_size() < len) {
        m_valid = false;
        m_enable = false;
        return;
    }
    m_X.resize(len);
    copy(X, X+len, m_X.begin());

    generateFittedLine();
}
