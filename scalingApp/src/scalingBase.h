#ifndef _scaling_base_h
#define _scaling_base_h

class scalingBase {
    public:
        scalingBase() {
            m_valid = false;
            m_enable = false;
        };
        virtual void setX(double * x, unsigned int len) = 0;
        virtual void setY(double * y, unsigned int len) = 0;
        virtual void setEnable(bool enable) {
            if (m_valid)
                m_enable = enable;
            else
                m_enable = false;
        };
        virtual bool isEnabled() {return m_enable;};
        virtual bool isValid() {return m_valid;};
        virtual double calibrate(double) = 0;
    protected:
        bool m_valid;
        bool m_enable;
};

#endif
