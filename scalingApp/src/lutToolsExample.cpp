#include <iocsh.h>
#include <epicsExport.h>
#include <string.h>
#include <stdexcept>

#include "lutToolsExample.h"
#include "lookupTable.h"

static const char *driverName="lutToolsExample";

lutToolsExample::lutToolsExample(const char *portName, const char* scalingFile) 
   : asynPortDriver(portName, 
                    1, /* maxAddr */ 
                    asynInt32Mask | asynFloat64Mask | asynFloat64ArrayMask | asynDrvUserMask | asynOctetMask, /* Interface mask */
                    asynInt32Mask | asynFloat64Mask | asynFloat64ArrayMask | asynOctetMask,  /* Interrupt mask */
                    0, /* asynFlags.  This driver does not block and it is not multi-device */
                    1, /* Autoconnect */
                    0, /* Default priority */
                    0) /* Default stack size*/
{
    createParam(P_EguValuesString,  asynParamFloat64Array,  &P_EguValues);
    createParam(P_RawValuesString,  asynParamFloat64Array,  &P_RawValues);
    createParam(P_LutString,        asynParamFloat64Array,  &P_Lut);
    createParam(P_FileNameString,   asynParamOctet,         &P_FileName);

    lutTools myLutObj;

    try {
        /* Try to read the file - the object will allocate memory for egu and raw arrays */
        myLutObj.readCsvFile(scalingFile);
    } catch (std::exception& e) {
        printf("--> Failed to load calibration file: %s\n", e.what());
        return;
    }

    /* file was loaded, get number of calibration points */
    _size = myLutObj.getNumPoints();
    printf("--> Calibration file with %d points\n", _size);

    /* Generate LUT */
    myLutObj.generateLookupTable(65536, (double*) _lutArray);

    /* Get EGU, RAW and LUT from the object and write them to array parameters*/
    //myLutObj.getLut((double*) _lutArray);
    _eguArray = new double[_size];
    _rawArray = new double[_size];
    myLutObj.getRaw((double*) _rawArray);
    myLutObj.getEgu((double*) _eguArray);

    printf("--> Data was read from file with success!\n");
}

lutToolsExample::~lutToolsExample() {
    delete[] _eguArray;
    delete[] _rawArray;
}

/** Called when asyn clients call pasynFloat64Array->read().
  * Returns the value of the P_Waveform or P_TimeBase arrays.  
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Pointer to the array to read.
  * \param[in] nElements Number of elements to read.
  * \param[out] nIn Number of elements actually read. */
asynStatus lutToolsExample::readFloat64Array(asynUser *pasynUser, epicsFloat64 *value, 
                                         size_t nElements, size_t *nIn)
{
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    epicsTimeStamp timeStamp;
    const char *functionName = "readFloat64Array";

    // Write the array PVs
    if (function == P_EguValues) {
        memcpy(value, (epicsFloat64*) _eguArray, _size*sizeof(epicsFloat64));
        *nIn = _size;       
    }

    else if (function == P_RawValues) {
        memcpy(value, (epicsFloat64*) _rawArray, _size*sizeof(epicsFloat64));
        *nIn = _size;       
    }

    else if (function == P_Lut) {
        memcpy(value, (epicsFloat64*) _lutArray, 65536*sizeof(epicsFloat64));
        *nIn = 65536;       
    }

    getTimeStamp(&timeStamp);
    pasynUser->timestamp = timeStamp;
    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, 
          "%s:%s: function=%d\n", 
          driverName, functionName, function);
    return status;
}


/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

int lutToolsExampleConfigure(const char *portName, const char *fileName)
{
    new lutToolsExample(portName, fileName);
    return(asynSuccess);
}

/* EPICS iocsh shell commands */

static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg initArg1 = { "scalingFile",iocshArgString};

static const iocshArg * const initArgs[] = {&initArg0, &initArg1};
static const iocshFuncDef initFuncDef = {"lutToolsExampleConfigure",2,initArgs};
static void initCallFunc(const iocshArgBuf *args)
{
    lutToolsExampleConfigure(args[0].sval, args[1].sval);
}

void lutToolsExampleRegister(void)
{
    iocshRegister(&initFuncDef,initCallFunc);
}

epicsExportRegistrar(lutToolsExampleRegister);

}

