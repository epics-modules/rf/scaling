#ifndef _scaling_poly_h
#define _scaling_poly_h

#include <gsl/gsl_multifit.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_poly.h>
#include <vector>
using namespace std;

#include "scalingBase.h"

class scalingPoly : public scalingBase {
    public:
        scalingPoly(unsigned int max_order):
            scalingBase (), 
            m_order(1), 
            m_max_order(max_order),
            m_chisq(0) {};
        virtual void setX(double * X, unsigned int len);
        virtual void setY(double * Y, unsigned int len);
        virtual int getOrder() {return m_order;};
        virtual void setOrder(unsigned int order);
        virtual double getChisq() {return m_chisq;};
        //this function doesn't check if is valid the calibration
        //so the call will be faster. It should be checked before
        //call the function
        virtual double calibrate(double value) { 
            return gsl_poly_eval(m_coefs.data(), m_order+1, value);
        }; 
        virtual double * getFittedLine() { return m_fitted_line.data();};
        virtual unsigned int getFittedLineSize() { return m_fitted_line.size();};
        virtual double * getResiduals() { return m_residuals.data();};
        virtual unsigned int getResidualsSize() { return m_residuals.size();};
        virtual double * getCoeffs() { return m_coefs.data();};
        virtual unsigned int getCoeffsSize() { return m_coefs.size();};
    protected:
        unsigned int m_order;
        unsigned int m_max_order;
        double m_chisq;
        vector<double> m_X;
        vector<double> m_Y;
        vector<double> m_coefs;
        vector<double> m_fitted_line;
        vector<double> m_residuals;

        virtual void fit();
        virtual void generateFittedLineAndResiduals();
};

#endif
