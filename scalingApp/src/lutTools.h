#ifndef _lut_tools_h
#define _lut_tools_h

#include <vector>

enum lutTools_types_t {
    lutTools_signed = 0x00,
    lutTools_unsigned = 0x01
};

enum lutTools_default_t {
    lutTools_raw_table,
    lutTools_voltage_table
};

class lutTools {
    public:
        int readCsvFile(const char* fileName);
        int getNumPoints(void);
        int generateLookupTable(int lutElements);
        int generateLookupTable(int lutElements, double *target);
        int getLut(double *target);
        int getRaw(double *target);
        int getEgu(double *target);
        int createLUT(lutTools_types_t type);
        lutTools(const std::string& filename, size_t lutsize = 65535, 
            lutTools_types_t type = lutTools_signed, 
            lutTools_default_t defaultvalues = lutTools_raw_table);
        lutTools();
        ~lutTools(void);
        std::vector<double> copyLUT();
        std::vector<double> copyRawPoints();
        std::vector<double> copyEguPoints();

    protected:
        double* mLutArray;
        double* mEguArray;
        double* mRawArray;
        int mNumPoints;
        int mLutElements;
    private:
        int readCsv(const char* fileName);
        int sortDataVectors();
        const size_t m_lutSize;       
        std::vector<double> m_LUT;
        std::vector<double> m_EguVector;
        std::vector<double> m_RawVector;
};

#endif /* _lut_tools_h */
