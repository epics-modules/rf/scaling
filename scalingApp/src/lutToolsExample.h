#ifndef _luttools_example_h
#define _luttools_example_h

/*
 * Test the lutFromFile class using a basic asynPortDriver
 * */

#include "asynPortDriver.h"

#define MAX_FILENAME_LEN        32

#define P_EguValuesString	    "EGUVALUES"
#define P_RawValuesString	    "RAWVALUES"
#define P_FileNameString	    "FILENAME"
#define P_LutString		        "LUT"

class lutToolsExample : public asynPortDriver {
public:
    lutToolsExample(const char *portName, const char* scalingFile);
    ~lutToolsExample();

    virtual asynStatus readFloat64Array(asynUser *pasynUser, epicsFloat64 *value,
                                    size_t nElements, size_t *nIn);
                 
protected:
    /** Values used for pasynUser->reason, and indexes into the parameter library. */
    int P_EguValues;
    int P_RawValues;
    int P_FileName;
    int P_Lut;
private:
    /* Our data */
    epicsFloat64 _lutArray[65536]; 
    epicsFloat64 *_eguArray;
    epicsFloat64 *_rawArray;
    int _size;
    int _lut_elements;
    char _fileName[MAX_FILENAME_LEN];
};

#endif /* _luttools_example_h */
