
#ifndef _scaling_poly_piecewise_h
#define _scaling_poly_piecewise_h

#include <algorithm>
#include "scalingPoly.h"
#include <gsl/gsl_multifit.h>

using namespace std;


class scalingPolyPiecewise : public scalingPoly{

protected:
    bool piecewise_calib_valid;
    bool piecewise_calib_en;
    const unsigned int MAX_INTERVALS;
    double chisq_piecewise;

    std::vector<double> starting_points;
    std::vector<int> polynomial_degrees;
    std::vector<gsl_multifit_linear_workspace*> workspaces;
    std::vector<std::vector<double>> coefficients;
    //new
    std::vector<double> m_fitted_line_piecewise;
    std::vector<double> m_residuals_piecewise;

    void fitPiecewise();
    void fitPolynomial(const std::vector<double>& sub_x, const std::vector<double>& sub_y, int degree);
    void generatePiecewiseFittedLineAndResiduals();
    int findIntervalIndex(double value) const;

public:
    scalingPolyPiecewise(unsigned int max_order, const int max_functions);
    ~scalingPolyPiecewise();

    // overrite from scalingPoly
    virtual void setX(double * X, unsigned int len);
    virtual void setY(double * Y, unsigned int len);
    double calibrate(double value);

    void setStartingPoints(double * starting_points, unsigned int len); //const std::vector<double>& startingPoints);
    void setPolynomialDegrees(int * degrees, unsigned int len); //const std::vector<int>& degrees);
    void printCoefficients() const;

    double * getFittedLinePiecewise() { return m_fitted_line_piecewise.data();};
    unsigned int getFittedLinePiecewiseSize() { return m_fitted_line_piecewise.size();};
    double * getResidualsPiecewise() { return m_residuals_piecewise.data();};
    unsigned int getResidualsPiecewiseSize() { return m_residuals_piecewise.size();};

    void setPolyCalibEnable(bool enable);
    bool getPolyCalibEnable(){
        return piecewise_calib_en;
    }
    bool getPolyCalibValid(){
        return piecewise_calib_valid;
    }
    double getChisqPiecewise() {
        return chisq_piecewise;
    };
};

#endif // _scaling_poly_piecewise_h

