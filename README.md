# scaling module

## ScalingLinear

A class to load csv and calculate a linear fitting

## ScalingNonLinear

A class to load csv and calculate all the lines between points
This class has a method to find the scaled value

## CSV file
* CSV file must have 2 columns, the first for EGU values and the second for Raw Values
* The first row will be ignored (is expected to be the title for columns)
* If there is more than one empty row at the end, everything downwards will be ignore
* An example is available in csv/example.csv


## Lookup Table Tools

This module offers a class named `lutTools` with methods than can generate an interpolation table from the CSV data. This interpolation table is meant to be used as a lookup table that translates raw data (ADC counts) into meaningful values (EGU). 

The algorithm that creates the table (LUT) is the following:
* Find the line equation between the first and second rows of the CSV data: `y = Ax + B` with `[A =((y2-y1)/(x2-x1))` and `B=y1-(A*x1)]`
* Fill the LUT from point zero until x2 (second X point in the CSV file) with this equation;
* Find a new equation between the subsequent segment and fill the LUT with the new values;
* Extend the last segment until the end of the LUT;

### Class Usage

The example below shows how one can use the `lutTools` class:

```
    #include "lookupTable.h"
    #include <stdexcept>

    ...

    // Allocate memory for your LUT
    double myArray[MAX_LUT_SIZE];
    
    // Instantiate lutTools;
    lutTools myLutObj;
    
    // Try to read the CSV file. lutTools with throw exceptions on any error
    try {
        myLutObj.readCsvFile("/path/to/file.csv");
    } catch (std::exception& e) {
        printf("Failed to read CSV file: %s\n", e.what());
        return -1;
    }

    // Generate the interpolation table and copy it to your array
    myLutObj.generateLookupTable(MAX_LUT_SIZE, myArray);

    // If one wants the data read from the CSV file
    int numPoints = myLutObj.getNumPoints();
    double *x_array = new double[numPoints];
    double *y_array = new double[numPoints];

    // Copy data into your array
    myLutObj.getRaw(x_array);
    myLutObj.getEgu(y_array);  

    //myLutObj destructor will free any memory once object goes out of scope
```

The `lutTools` class is not dependent on EPICS, it's just a simple tool that can be used inside an EPICS driver code. It's designed in such way so the manipulation of the CSV file remains inside the IOC commands, not exposed as PV. 

