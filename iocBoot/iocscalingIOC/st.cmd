#!../../bin/linux-x86_64/scaling

## You may have to change scalingIOC to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/scaling.dbd"
scaling_registerRecordDeviceDriver pdbbase

scalingConfigure("scalingTest")

## Load record instances
dbLoadRecords("db/scaling.db","PREFIX=TESTSCALING,PORT=scalingTest,ADDR=0,TIMEOUT=1,MAX_ELEM=1024")

cd "${TOP}/iocBoot/${IOC}"
iocInit
